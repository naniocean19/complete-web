const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");

app.set("port", 8080);
app.use(bodyParser.json({ type: "application/json" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
const Pool = require("pg").Pool;
const config = {
        host: "localhost",
        user: "serve",
        password: "frankie19",
        database: "food_nutrition"

};

const pool = new Pool(config);

app.get("/api/info", async (req, res) => {
        console.log("here");
        try{
        const template = "SELECT description, kcal,sodium_mg, protein_g, lipid_total_g FROM entries WHERE description ILIKE $1 LIMIT 25";
        const response = await pool.query(template, ["%"+req.query.q+"%"]);
        console.log("here2");
		if (response.rowCount == 0){
                      res.json([]);
                }
        else{
               console.log("here3");
		const n = response.rows.map(function(item) {
                                        let x =  item.lipid_total_g / item.kcal;

                                        return ({description: item.description, kcal: item.kcal, sodium: item.sodium_mg, protein: item.protein_g, fat: x.toFixed(2)});
                        });



                        console.log(n);
                        res.json(n);

        }
        }
        catch(err) {
                res.json({err});
        }
});
app.get("/hello", (req, res) => {
        res.json("Hello World!");
});
app.listen(app.get("port"), () => {
        console.log(`Find the server`);
});
