import Layout from "../components/Layout";

import {getInfo} from '../lib/utils'
import React from "react";

class Index extends React.Component {
  constructor(props) {
    super(props);
    this.styles = {
      result: {
               'padding-left': '10%',
               'padding-right': '10%',
               'display': 'flex',
               'justify-content': 'space-around',
               'text-align': 'left'
          },

          resultCat: {
               'width': '20%',
               'float': 'left',
               'text-align': 'left',
               'padding-left': '5%',
               'padding-right': '5%'
          },

          resultsHeader: {
               'padding-left': '10%',
               'padding-right': '10%',
               'display': 'flex',
               'justify-content': 'space-around'
          },
          category: {
               'padding-left': '5%',
               'padding-right': '5%',
               'width': '20%',
               'float': 'left',
               'font-weight': 'bold',
               'text-align': 'left'
           }

    }
    this.state={search: 'search here'}; 
    let oput = "";
    this.response= [0,1]; 
    
    
  }
  handleUpdate(evt){

    this.setState({search: evt.target.value});

  }
async handleSearch(evt) {
  this.setState({search: evt.target.value});
  const user = await getInfo(evt.target.value);
  this.setState({user}); 
   }

  render() {
    return (
      <div style={{ margin: 20, padding: 20, border: '1px solid #DDD', textAlign: "center" }}>
        <Layout />
        <h3>Food Search</h3>
        <p><input type='text' value={this.state.search} onChange={this.handleSearch.bind(this)} /></p>
        <div style = {this.styles.resultsHeader}> 
          <p sytle = {this.styles.category} >Descrption</p> 
          <p sytle = {this.styles.category} >Calories</p> 
          <p sytle = {this.styles.category}>Sodium (mg) </p> 
          <p sytle = {this.styles.category}>Protein (mg) </p> 
          <p sytle = {this.styles.category}> Total Fat (%)</p> 
        </div> 
         
    {this.state.user  ? this.state.user.map((item, index) => {
      return(
        
                       <div style={this.styles.result}>
                           <p style={this.styles.resultCat} >{item.description}</p>
                           <p style={this.styles.resultCat}>{item.kcal}</p>
                           <p style={this.styles.resultCat} >{item.sodium}</p>
                           <p style={this.styles.resultCat}>{item.protein}</p>
                           <p style={this.styles.resultCat}>{item.fat}</p>
                          
                       </div>
                       
               );
        
    }) : null
    }
        <style jsx>{`
          h1,
          h2,
          a,
          p {
            font-family: "Optima";
          }
          .button-style {
        margin: auto auto;
        cursor: pointer;
        background-color: #4633FF;
        color: #ffffff;
        width: 100px;
        font-family: "Arial";
      }
      .result: {
               'padding-left': '10%',
               'padding-right': '10%',
               'display': 'flex',
               'justify-content': 'space-around',
               'text-align': 'left'
           }

           .resultCat: {
               'width': '20%',
               'float': 'left',
               'text-align': 'left',
               'padding-left': '5%',
               'padding-right': '5%'
           }

.resultsHeader: {
               'padding-left': '10%',
               'padding-right': '10%',
               'display': 'flex',
               'justify-content': 'space-around'
           }
           .category: {
               'padding-left': '5%',
               'padding-right': '5%',
               'width': '20%',
               'float': 'left',
               'font-weight': 'bold',
               'text-align': 'left'
           }

      .resultsd{

         width: 20%
         display: inline;
         padding-left: 5%;
        padding-right: 5%;
    }
          .description {
            font-family: "Optima";
            font-size: "10px";
          }
          ul {
            padding: 0;
          }
          li {
            list-style: none;
            margin: 5px 0;
          }
          .App-logo {
            height: 80px;
          }
          a {
            text-decoration: none;
            color: green;
          }
          a:hover {
            opacity: 0.6;
          }
        `}</style>
      </div>
);
  }
}
export default Index;
                            
