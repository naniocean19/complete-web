module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Header.js":
/*!******************************!*\
  !*** ./components/Header.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/naniocean19/350/web2/front/components/Header.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
const styles = {
  image: {
    width: '100px',
    height: '100px'
  },
  headerStyle: {
    backgroundColor: "black",
    color: "green",
    width: "100%",
    height: "130px",
    textAlign: "center"
  }
};

const Header = () => __jsx("div", {
  className: "Header",
  style: styles.headerStyle,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 15
  },
  __self: undefined
}, __jsx("img", {
  style: styles.image,
  src: "/static/logo512.png ",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 16
  },
  __self: undefined
}), __jsx("h2", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 18
  },
  __self: undefined
}, " Nutritional Information About Food "));

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ }),

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Header */ "./components/Header.js");
/* harmony import */ var _NavBar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./NavBar */ "./components/NavBar.js");
var _jsxFileName = "/home/naniocean19/350/web2/front/components/Layout.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const layoutStyle = {
  display: "flex",
  flexDirection: "column",
  height: "100%",
  width: "100%"
};
const contentStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column"
};

const Layout = props => __jsx("div", {
  className: "Layout",
  style: layoutStyle,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 18
  },
  __self: undefined
}, __jsx(_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 19
  },
  __self: undefined
}), __jsx("div", {
  className: "Content",
  style: contentStyle,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 20
  },
  __self: undefined
}, props.children));

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/NavBar.js":
/*!******************************!*\
  !*** ./components/NavBar.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var _jsxFileName = "/home/naniocean19/350/web2/front/components/NavBar.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
const navBarStyle = {
  backgroundColor: "red",
  color: "white",
  width: "100%",
  height: "60px"
};

const NavBar = () => __jsx("div", {
  className: "NavBar",
  style: navBarStyle,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 9
  },
  __self: undefined
}, "NAVBAR");

/* harmony default export */ __webpack_exports__["default"] = (NavBar);

/***/ }),

/***/ "./lib/utils.js":
/*!**********************!*\
  !*** ./lib/utils.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! isomorphic-fetch */ "isomorphic-fetch");

function getProfile(username) {
  console.log("hello");
  return fetch(`http://35.196.121.233/api/info?q=${username}`).then(function (resp) {
    return resp.json();
  });
}

function handleError(error) {
  console.warn(error);
  return null;
}

module.exports = {
  getInfo: function (user) {
    return getProfile(user).catch(handleError);
  }
};

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _lib_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../lib/utils */ "./lib/utils.js");
/* harmony import */ var _lib_utils__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_lib_utils__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/home/naniocean19/350/web2/front/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_3___default.a.createElement;




class Home extends react__WEBPACK_IMPORTED_MODULE_3___default.a.Component {
  constructor(props) {
    super(props);
    this.styles = {
      result: {
        'padding-left': '10%',
        'padding-right': '10%',
        'display': 'flex',
        'justify-content': 'space-around',
        'text-align': 'left'
      },
      resultCat: {
        'width': '20%',
        'float': 'left',
        'text-align': 'left',
        'padding-left': '5%',
        'padding-right': '5%'
      },
      resultsHeader: {
        'padding-left': '10%',
        'padding-right': '10%',
        'display': 'flex',
        'justify-content': 'space-around'
      },
      category: {
        'padding-left': '5%',
        'padding-right': '5%',
        'width': '20%',
        'float': 'left',
        'font-weight': 'bold',
        'text-align': 'left'
      }
    };
    this.state = {
      search: 'search here'
    };
    let oput = "";
    this.response = [0, 1];
  }

  handleUpdate(evt) {
    this.setState({
      search: evt.target.value
    });
  }

  async handleSearch(evt) {
    this.setState({
      search: evt.target.value
    });
    const user = await Object(_lib_utils__WEBPACK_IMPORTED_MODULE_2__["getInfo"])(evt.target.value);
    this.setState({
      user
    });
  }

  render() {
    return __jsx("div", {
      style: {
        margin: 20,
        padding: 20,
        border: '1px solid #DDD',
        textAlign: "center"
      },
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 61
      },
      __self: this
    }, __jsx(_components_Layout__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 62
      },
      __self: this
    }), __jsx("h3", {
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 63
      },
      __self: this
    }, "Food Search"), __jsx("p", {
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    }, __jsx("input", {
      type: "text",
      value: this.state.search,
      onChange: this.handleSearch.bind(this),
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 64
      },
      __self: this
    })), __jsx("div", {
      style: this.styles.resultsHeader,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 65
      },
      __self: this
    }, __jsx("p", {
      sytle: this.styles.category,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 66
      },
      __self: this
    }, "Descrption"), __jsx("p", {
      sytle: this.styles.category,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 67
      },
      __self: this
    }, "Calories"), __jsx("p", {
      sytle: this.styles.category,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 68
      },
      __self: this
    }, "Sodium (mg) "), __jsx("p", {
      sytle: this.styles.category,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 69
      },
      __self: this
    }, "Protein (mg) "), __jsx("p", {
      sytle: this.styles.category,
      className: "jsx-505146562",
      __source: {
        fileName: _jsxFileName,
        lineNumber: 70
      },
      __self: this
    }, " Total Fat (%)")), this.state.user ? this.state.user.map((item, index) => {
      return __jsx("div", {
        style: this.styles.result,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, __jsx("p", {
        style: this.styles.resultCat,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }, item.description), __jsx("p", {
        style: this.styles.resultCat,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      }, item.kcal), __jsx("p", {
        style: this.styles.resultCat,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, item.sodium), __jsx("p", {
        style: this.styles.resultCat,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        },
        __self: this
      }, item.protein), __jsx("p", {
        style: this.styles.resultCat,
        className: "jsx-505146562",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, item.fat));
    }) : null, __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
      id: "505146562",
      __self: this
    }, "h1.jsx-505146562,h2.jsx-505146562,a.jsx-505146562,p.jsx-505146562{font-family:\"Optima\";}.button-style.jsx-505146562{margin:auto auto;cursor:pointer;background-color:#4633FF;color:#ffffff;width:100px;font-family:\"Arial\";}.result.jsx-505146562:{'padding-left':'10%', 'padding-right':'10%', 'display':'flex', 'justify-content':'space-around', 'text-align':'left';}.resultCat.jsx-505146562:{'width':'20%', 'float':'left', 'text-align':'left', 'padding-left':'5%', 'padding-right':'5%';}.resultsHeader.jsx-505146562:{'padding-left':'10%', 'padding-right':'10%', 'display':'flex', 'justify-content':'space-around';}.category.jsx-505146562:{'padding-left':'5%', 'padding-right':'5%', 'width':'20%', 'float':'left', 'font-weight':'bold', 'text-align':'left';}.resultsd.jsx-505146562{width:20% display:inline;padding-left:5%;padding-right:5%;}.description.jsx-505146562{font-family:\"Optima\";font-size:\"10px\";}ul.jsx-505146562{padding:0;}li.jsx-505146562{list-style:none;margin:5px 0;}.App-logo.jsx-505146562{height:80px;}a.jsx-505146562{-webkit-text-decoration:none;text-decoration:none;color:green;}a.jsx-505146562:hover{opacity:0.6;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25hbmlvY2VhbjE5LzM1MC93ZWIyL2Zyb250L3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXdGb0IsQUFNa0MsQUFHUixBQWFmLEFBUUEsQUFPQSxBQVFBLEFBS2MsQUFLUyxBQUlYLEFBR00sQUFJSixBQUdTLEFBSVQsVUFiZCxFQU9BLEFBT0EsSUFYZSxDQXJERixJQUhiLEFBaURtQixJQUxKLElBYWYsR0FyRHVCLE1BOEN2QixHQUxlLFNBa0JELE9BMURGLENBeUNsQixJQWtCTSxTQTFEVSxZQUNRLFdBZ0JqQixFQU9BLE9BdEJMLGFBOEJLLENBdkJBIiwiZmlsZSI6Ii9ob21lL25hbmlvY2VhbjE5LzM1MC93ZWIyL2Zyb250L3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IExheW91dCBmcm9tIFwiLi4vY29tcG9uZW50cy9MYXlvdXRcIjtcblxuaW1wb3J0IHtnZXRJbmZvfSBmcm9tICcuLi9saWIvdXRpbHMnXG5pbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmNsYXNzIEhvbWUgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0eWxlcyA9IHtcbiAgICAgIHJlc3VsdDoge1xuICAgICAgICAgICAgICAgJ3BhZGRpbmctbGVmdCc6ICcxMCUnLFxuICAgICAgICAgICAgICAgJ3BhZGRpbmctcmlnaHQnOiAnMTAlJyxcbiAgICAgICAgICAgICAgICdkaXNwbGF5JzogJ2ZsZXgnLFxuICAgICAgICAgICAgICAgJ2p1c3RpZnktY29udGVudCc6ICdzcGFjZS1hcm91bmQnLFxuICAgICAgICAgICAgICAgJ3RleHQtYWxpZ24nOiAnbGVmdCdcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgcmVzdWx0Q2F0OiB7XG4gICAgICAgICAgICAgICAnd2lkdGgnOiAnMjAlJyxcbiAgICAgICAgICAgICAgICdmbG9hdCc6ICdsZWZ0JyxcbiAgICAgICAgICAgICAgICd0ZXh0LWFsaWduJzogJ2xlZnQnLFxuICAgICAgICAgICAgICAgJ3BhZGRpbmctbGVmdCc6ICc1JScsXG4gICAgICAgICAgICAgICAncGFkZGluZy1yaWdodCc6ICc1JSdcbiAgICAgICAgICB9LFxuXG4gICAgICAgICAgcmVzdWx0c0hlYWRlcjoge1xuICAgICAgICAgICAgICAgJ3BhZGRpbmctbGVmdCc6ICcxMCUnLFxuICAgICAgICAgICAgICAgJ3BhZGRpbmctcmlnaHQnOiAnMTAlJyxcbiAgICAgICAgICAgICAgICdkaXNwbGF5JzogJ2ZsZXgnLFxuICAgICAgICAgICAgICAgJ2p1c3RpZnktY29udGVudCc6ICdzcGFjZS1hcm91bmQnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBjYXRlZ29yeToge1xuICAgICAgICAgICAgICAgJ3BhZGRpbmctbGVmdCc6ICc1JScsXG4gICAgICAgICAgICAgICAncGFkZGluZy1yaWdodCc6ICc1JScsXG4gICAgICAgICAgICAgICAnd2lkdGgnOiAnMjAlJyxcbiAgICAgICAgICAgICAgICdmbG9hdCc6ICdsZWZ0JyxcbiAgICAgICAgICAgICAgICdmb250LXdlaWdodCc6ICdib2xkJyxcbiAgICAgICAgICAgICAgICd0ZXh0LWFsaWduJzogJ2xlZnQnXG4gICAgICAgICAgIH1cblxuICAgIH1cbiAgICB0aGlzLnN0YXRlPXtzZWFyY2g6ICdzZWFyY2ggaGVyZSd9OyBcbiAgICBsZXQgb3B1dCA9IFwiXCI7XG4gICAgdGhpcy5yZXNwb25zZT0gWzAsMV07IFxuICAgIFxuICAgIFxuICB9XG4gIGhhbmRsZVVwZGF0ZShldnQpe1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7c2VhcmNoOiBldnQudGFyZ2V0LnZhbHVlfSk7XG5cbiAgfVxuYXN5bmMgaGFuZGxlU2VhcmNoKGV2dCkge1xuICB0aGlzLnNldFN0YXRlKHtzZWFyY2g6IGV2dC50YXJnZXQudmFsdWV9KTtcbiAgY29uc3QgdXNlciA9IGF3YWl0IGdldEluZm8oZXZ0LnRhcmdldC52YWx1ZSk7XG4gIHRoaXMuc2V0U3RhdGUoe3VzZXJ9KTsgXG4gICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IHN0eWxlPXt7IG1hcmdpbjogMjAsIHBhZGRpbmc6IDIwLCBib3JkZXI6ICcxcHggc29saWQgI0RERCcsIHRleHRBbGlnbjogXCJjZW50ZXJcIiB9fT5cbiAgICAgICAgPExheW91dCAvPlxuICAgICAgICA8aDM+Rm9vZCBTZWFyY2g8L2gzPlxuICAgICAgICA8cD48aW5wdXQgdHlwZT0ndGV4dCcgdmFsdWU9e3RoaXMuc3RhdGUuc2VhcmNofSBvbkNoYW5nZT17dGhpcy5oYW5kbGVTZWFyY2guYmluZCh0aGlzKX0gLz48L3A+XG4gICAgICAgIDxkaXYgc3R5bGUgPSB7dGhpcy5zdHlsZXMucmVzdWx0c0hlYWRlcn0+IFxuICAgICAgICAgIDxwIHN5dGxlID0ge3RoaXMuc3R5bGVzLmNhdGVnb3J5fSA+RGVzY3JwdGlvbjwvcD4gXG4gICAgICAgICAgPHAgc3l0bGUgPSB7dGhpcy5zdHlsZXMuY2F0ZWdvcnl9ID5DYWxvcmllczwvcD4gXG4gICAgICAgICAgPHAgc3l0bGUgPSB7dGhpcy5zdHlsZXMuY2F0ZWdvcnl9PlNvZGl1bSAobWcpIDwvcD4gXG4gICAgICAgICAgPHAgc3l0bGUgPSB7dGhpcy5zdHlsZXMuY2F0ZWdvcnl9PlByb3RlaW4gKG1nKSA8L3A+IFxuICAgICAgICAgIDxwIHN5dGxlID0ge3RoaXMuc3R5bGVzLmNhdGVnb3J5fT4gVG90YWwgRmF0ICglKTwvcD4gXG4gICAgICAgIDwvZGl2PiBcbiAgICAgICAgIFxuICAgIHt0aGlzLnN0YXRlLnVzZXIgID8gdGhpcy5zdGF0ZS51c2VyLm1hcCgoaXRlbSwgaW5kZXgpID0+IHtcbiAgICAgIHJldHVybihcbiAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3RoaXMuc3R5bGVzLnJlc3VsdH0+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBzdHlsZT17dGhpcy5zdHlsZXMucmVzdWx0Q2F0fSA+e2l0ZW0uZGVzY3JpcHRpb259PC9wPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgc3R5bGU9e3RoaXMuc3R5bGVzLnJlc3VsdENhdH0+e2l0ZW0ua2NhbH08L3A+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBzdHlsZT17dGhpcy5zdHlsZXMucmVzdWx0Q2F0fSA+e2l0ZW0uc29kaXVtfTwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIHN0eWxlPXt0aGlzLnN0eWxlcy5yZXN1bHRDYXR9PntpdGVtLnByb3RlaW59PC9wPlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgPHAgc3R5bGU9e3RoaXMuc3R5bGVzLnJlc3VsdENhdH0+e2l0ZW0uZmF0fTwvcD5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICk7XG4gICAgICAgIFxuICAgIH0pIDogbnVsbFxuICAgIH1cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgIGgxLFxuICAgICAgICAgIGgyLFxuICAgICAgICAgIGEsXG4gICAgICAgICAgcCB7XG4gICAgICAgICAgICBmb250LWZhbWlseTogXCJPcHRpbWFcIjtcbiAgICAgICAgICB9XG4gICAgICAgICAgLmJ1dHRvbi1zdHlsZSB7XG4gICAgICAgIG1hcmdpbjogYXV0byBhdXRvO1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM0NjMzRkY7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgICAgICB3aWR0aDogMTAwcHg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIkFyaWFsXCI7XG4gICAgICB9XG4gICAgICAucmVzdWx0OiB7XG4gICAgICAgICAgICAgICAncGFkZGluZy1sZWZ0JzogJzEwJScsXG4gICAgICAgICAgICAgICAncGFkZGluZy1yaWdodCc6ICcxMCUnLFxuICAgICAgICAgICAgICAgJ2Rpc3BsYXknOiAnZmxleCcsXG4gICAgICAgICAgICAgICAnanVzdGlmeS1jb250ZW50JzogJ3NwYWNlLWFyb3VuZCcsXG4gICAgICAgICAgICAgICAndGV4dC1hbGlnbic6ICdsZWZ0J1xuICAgICAgICAgICB9XG5cbiAgICAgICAgICAgLnJlc3VsdENhdDoge1xuICAgICAgICAgICAgICAgJ3dpZHRoJzogJzIwJScsXG4gICAgICAgICAgICAgICAnZmxvYXQnOiAnbGVmdCcsXG4gICAgICAgICAgICAgICAndGV4dC1hbGlnbic6ICdsZWZ0JyxcbiAgICAgICAgICAgICAgICdwYWRkaW5nLWxlZnQnOiAnNSUnLFxuICAgICAgICAgICAgICAgJ3BhZGRpbmctcmlnaHQnOiAnNSUnXG4gICAgICAgICAgIH1cblxuLnJlc3VsdHNIZWFkZXI6IHtcbiAgICAgICAgICAgICAgICdwYWRkaW5nLWxlZnQnOiAnMTAlJyxcbiAgICAgICAgICAgICAgICdwYWRkaW5nLXJpZ2h0JzogJzEwJScsXG4gICAgICAgICAgICAgICAnZGlzcGxheSc6ICdmbGV4JyxcbiAgICAgICAgICAgICAgICdqdXN0aWZ5LWNvbnRlbnQnOiAnc3BhY2UtYXJvdW5kJ1xuICAgICAgICAgICB9XG4gICAgICAgICAgIC5jYXRlZ29yeToge1xuICAgICAgICAgICAgICAgJ3BhZGRpbmctbGVmdCc6ICc1JScsXG4gICAgICAgICAgICAgICAncGFkZGluZy1yaWdodCc6ICc1JScsXG4gICAgICAgICAgICAgICAnd2lkdGgnOiAnMjAlJyxcbiAgICAgICAgICAgICAgICdmbG9hdCc6ICdsZWZ0JyxcbiAgICAgICAgICAgICAgICdmb250LXdlaWdodCc6ICdib2xkJyxcbiAgICAgICAgICAgICAgICd0ZXh0LWFsaWduJzogJ2xlZnQnXG4gICAgICAgICAgIH1cblxuICAgICAgLnJlc3VsdHNke1xuXG4gICAgICAgICB3aWR0aDogMjAlXG4gICAgICAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgICAgICBwYWRkaW5nLWxlZnQ6IDUlO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA1JTtcbiAgICB9XG4gICAgICAgICAgLmRlc2NyaXB0aW9uIHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wdGltYVwiO1xuICAgICAgICAgICAgZm9udC1zaXplOiBcIjEwcHhcIjtcbiAgICAgICAgICB9XG4gICAgICAgICAgdWwge1xuICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICB9XG4gICAgICAgICAgbGkge1xuICAgICAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgICAgIG1hcmdpbjogNXB4IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5BcHAtbG9nbyB7XG4gICAgICAgICAgICBoZWlnaHQ6IDgwcHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIGEge1xuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgICAgY29sb3I6IGdyZWVuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBhOmhvdmVyIHtcbiAgICAgICAgICAgIG9wYWNpdHk6IDAuNjtcbiAgICAgICAgICB9XG4gICAgICAgIGB9PC9zdHlsZT5cbiAgICAgIDwvZGl2PlxuKTtcbiAgfVxufVxuZXhwb3J0IGRlZmF1bHQgSG9tZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiJdfQ== */\n/*@ sourceURL=/home/naniocean19/350/web2/front/pages/index.js */"));
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ 4:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/naniocean19/350/web2/front/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "isomorphic-fetch":
/*!***********************************!*\
  !*** external "isomorphic-fetch" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-fetch");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map