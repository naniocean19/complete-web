const styles = {
	image: {
		width: '100px',
		height: '100px'
	},

  headerStyle: {backgroundColor: "black",
    color: "green",
    width: "100%",
    height: "130px",
  textAlign: "center"}
};

const Header = () => (
  <div className="Header" style={styles.headerStyle}>
<img style={styles.image}  src="/static/logo512.png "/>

<h2> Nutritional Information About Food </h2> 
  </div>
);

export default Header;

